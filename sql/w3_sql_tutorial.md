# SQL Tutorial

## SQL Introduction

SQL is a standard language for accessing and manipulating databases.

### What is SQL?

- SQL can:
    + execute queries against a database
    + retrieve data from a database
    + insert records in a database
    + update records in a database
    + delete records from a database
    + create new databases
    + create new tables in a database
    + create stored procedures in a database
    + create views in a database
    + set permission on tables, procedures, and views
- SQL is an ANSI standard
    + but there are different versions of the language
        * they are still complient and therefore support major commands
            - SELECT, UPDATE, INSERT, WHERE
- RDBMS is the basis for SQL
    + data in RDBMS is stored in database objects called tables

RDBMS
: Relational Database Management System

Table
: a collection of related data entries that consists of columns and rows.

## SQL Syntax

### Database Tables

- databases most often contain one or more tables
    + each table is identified by name (e.g "Customers" or "Orders")
        * tables contain records (rows) with data

**Customers table**

| CustomerID |   CustomerName  | ContactName  |        Address         |    City    | PostalCode | Country |
|------------|-----------------|--------------|------------------------|------------|------------|---------|
|          1 | Alfred          | Maria        | Obere Str.             | Berlin     | 12209      | Germany |
|          2 | Ana             | Ana          | Avda. del Constitucion | Mexico D.F | 05021      | Mexico  |
|          3 | Antonio         | Antonio      | Mataderos              | Mexico D.F | 05023      | Mexico  |
|          4 | Around the Horn | Thomas Hardy | 120 Hanover            | London     | WA1 1DP    | UK      |
|          5 | Berglunds       | Christina    | Berguvsvangen          | Lulea      | S-958      | Sweden  |

The table above contains 5 records (one for each customer) and seven columns (one for each attribute)

### SQL Statements

Most of the actions you need to perform on a database are done with SQL statements

**Select all the records in the "Customers" table:**

```sql
SELECT * FROM Customers;
```

- SQL is NOT case sensitive
    + select is the same as SELECT
    + most of the time keywords are written in upper-case
- some databases require a semicolon at the end of each SQL statement
    + semicolon is the standard seperate SQL statements

**List of The Most Important SQL Commands**

- **SELECT** - extracts data from a database
- **UPDATE** - updates data in a database
- **DELETE** - deletes data from a database
- **INSERT INTO** - inserts new data into a database
- **CREATE DATABASE** - creates a new database
- **ALTER DATABASE** - modifies a database
- **CREATE TABLE** - creates a new table
- **DROP TABLE** - deletes a table
- **CREATE INDEX** - creates an index (search key)
- **DROP INDEX** - deletes an index

## SQL *CREATE DATABASE* Statement

- CREATE DATABASE statement is used to create a database

Create a new database called "my_db":
```sql
CREATE DATABASE my_db;
```

## SQL *CREATE TABLE* Statement:

- CREATE TABLE statment is used to create a table in a database
    + tables are organized into rows and columns; and each must have a name

Syntax:
```
CREATE TABLE table_name
(
    column_name1 data_type(size)
    column_name2 data_type(size)
    column_name3 data_type(size)
    ...
);
```

- column_name parameters specify the names of the columns of the table
- data_type parameter specifies what type of data the column can hold
    + varchar, integer, decimal, date, etc
- size parameter specifies the max length of the column of the table

Create a tabled called "Person":
```sql
CREATE TABLE Persons
(
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255),
    DateOfBirth date
)
```

## SQL *Constraints*

**Constraints:**

- are used to specify rules for the data in a table
    + if there is any violation between the constraint and the data action, the action is aborted by the constraint
- can be specified when the table is created (CREATE TABLE)
- can be specified after the table is created (ALTER TABLE)

Syntax:
```sql
CREATE TABLE table_name
(
    column_name1 data_type(size) constraint_name,
    column_name2 data_type(size) constraint_name,
    ...
)
```

**List of Constraints:**

- **NOT NULL** - indicates that a column cannot store NULL value
- **UNIQUE** - ensures that each row for a column must have a unique value
- **PRIMARY KEY** - a combination of NOT NULL and UNIQUE
- **FOREIGN KEY** - ensures the referential integrity of the data in one table to match values in another table
- **CHECK** - ensures that the value in a column meets a specific condition
- **DEFAULT** - Specifies a default value when specified none for this column

## SQL *FOREIGN KEY* Constraint

- FOREIGN KEY in one table points to a PRIMARY KEY in another table

"Persons" Table:

| P_ID |  LastName | FirstName |
|------|-----------|-----------|
|    1 | Hansen    | Ola       |
|    2 | Svendson  | Tove      |
|    3 | Pettersen | Kari      |

"Orders" Table:

| O_ID | OrderNo | P_ID |
|------|---------|------|
|    1 |      77 |    3 |
|    2 |      88 |    3 |
|    3 |      99 |    2 |
|    4 |     111 |    1 |

CREATE TABLE for "Orders" table:
```sql
CREATE TABLE Orders
(
    O_ID int NOT NULL PRIMARY KEY,
    OrderNo int NOT NULL,
    P_ID int FOREIGN KEY REFERENCES Persons(P_ID)
);
```

## SQL *CHECK* Constraint

- CHECK constraint is used to limit the value range that can be placed in a column
- define a CHECK on a single column
    + allows only certain values for this column
- define a CHECK on a table
    + it can limit the values in certain columns based on values in other column in row

CHECK constraint on the "P_Id" column when the "Persons" table is created; P_Id can only store integres greater than 0.
```sql
CREATE TABLE Persons
(
    P_Id int NOT NULL CHECK (P_Id>0),
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255)
);
```

Name a CHECK constraint, and define a CHECK constraint on multiple columns:
```sql
CREATE TABLE Persons
(
    P_Id int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255),
    CONSTRAINT chk_Person CHECK (P_Id>0 AND City='Chicago')
);
```

## SQL *SELECT* Statement

- SELECT statement is used to select data from a database.
    + the result is stored ina result table, called result-set

Select the "CustomerName" and "City" columns from the "Customers" table:
```sql
SELECT CustomerName, City
FROM Customers;
```

Select all the columns fro the "Customers" table:
```sql
SELECT * FROM Customers;
```

## SQL *SELECT DISTINCT* Statement

- DISTINCT keyword can be used to return only distinct (different) values.
- a column in a table may contain duplicate values;
    + sometimes you only want to list the distinct values

Select the distinct values from the"CustomerName" and "City" columns from the "Customers" table:
```sql
SELECT DISTINCT CustomerName, City
FROM Customers;
```

## SQL *WHERE* Clause

- WHERE clause is used to filter records by only returning records that fulfill a specified criterion

Select all the customers from the country "Mexico" from the "Customers" table:
```sql
SELECT *
FROM Customers
WHERE Country='Mexico';
```

Select all the customers with "CustomerID" equal to 1 from the "Customers" table:
```sql
SELECT *
FROM Customers
WHERE CustomerID=1;
```

**WHERE Clause Operators**

| Operator |                   Description                    |
|----------|--------------------------------------------------|
| =        | Equal                                            |
| <>       | Not equal. **Note:** Some versions of SQL use != |
| >        | Greater than                                     |
| <        | Less than                                        |
| >=       | Greater than or equal                            |
| <=       | Less than or equal                               |
| BETWEEN  | Between an inclusive range                       |
| LIKE     | Seach for a patter                               |
| IN       | To specify multiple possible values for a column |

## SQL *AND & OR* Operators

- AND operator displays a record if both the first AND second condition are true
- OR operator displays a record if either the first OR second condition is true

Select all the customers from the country "Germany" AND city "Berlin" from the "Customers" table:
```sql
SELECT *
FROM Customers
WHERE Country='Germany'
AND City='Berlin';
```

Select all the customers from the country "Germany" OR "Mexico" from the "Customers" table:
```sql
SELECT *
FROM Customers
WHERE Country='Germany'
OR Country='Mexico';
```

Select all the customers from the country "Germany" who live in "Berlin" OR "Munchen" from the "Customers" table:
```sql
SELECT *
FROM Customers
WHERE Country='Germany'
AND (City='Berlin' OR City='Munchen');
```

## SQL *ORDER BY* Keyword

- ORDER BY keyword is used to sort the result-set by one or more columns
    + sorts in ascending order by default
        * use DESC keyword to sort in descending order

Select all the customers from the "Customers" table, sorted by the "Country" column:
```sql
SELECT *
FROM Customers
ORDER BY Country;
```

Same as above but in descending order:
```sql
SELECT *
FROM Customers
ORDER BY Country, CustomerName DESC;
```


## SQL *JOINS*

- joins are used to combine rows from tow or more tables, based on a common field between them

**List of SQL JOINs**

- **INNER JOIN** - Returns all rows when there is at least one match in BOTH tables
- **LEFT JOIN** - Returns all rows from the left table, and the matched rows from the right table
- **RIGHT JOIN** - Returns all rows from the right table, and the matched rows from the left table
- **FULL JOIN** - Returns all rows when there is a match in ONE of the tables

## SQL *INNER JOIN* Keyword

- INNER JOIN keyword selects all rows from both tables as long as there is a match between the columns in both tables
- JOIN and INNER JOIN are the same

Syntax:
```sql
SELECT column_name(s)
FROM table1
INNER JOIN table2
ON table1.column_name=table2.column_name;
```

Return all customers with orders:
```sql
SELECT Customers.CustomerName, Orders.OrderID
FROM Customers
INNER JOIN Orders
ON Customers.CustomerId=Orders.CustomerID;
```

## SQL *LEFT JOIN* Keyword

- LEFT JOIN keyword selects all rows from the left table(table 1), with matching rows in the right table (table 2)
- returns all the rows from the left table, even if there are not matches in the right table, which returns NULL
- LEFT JOIN is same as LEFT OUTER JOIN

Syntax:
```sql
SELECT column_name(s)
FROM table1
LEFT JOIN table2
ON table1.column_name=table2.column_name;
```

Return all customers with orders:
```sql
SELECT Customers.CustomerName, Orders.OrderID
FROM Customers
INNER JOIN Orders
ON Customers.CustomerId=Orders.CustomerID;
```

## SQL UNION Operator

- UNION operator is used to combine the result-set of two or more SELECT statements
- statements must have the same number of columns
- statements must also have similar data types
- columns in each SELECT statement must be in the same order

Syntax:
```sql
SELECT column_name(s) FROM table1
UNION
SELECT column_name(s) FROM table2;
```

## SQL *VIEWS*

**View**

- A view contains rows and columns, just like a real table
- fields in a view are fields from one or more real tables in a database
- views show up-to-date data

Create a view:

```sql
CREATE VIEW view_name AS
SELECT column_name(s)
FROM table_name
WHERE condition;
```

```sql
CREATE VIEW [Current Product List] AS
SELECT ProductID, ProductName
From Products
WHERE Discountinued=No;
```

```sql
SELECT * FROM [Current Product List];
```

## SQL *INSERT INTO* Statement

- INSERT INTO statement is used to insert new records in a table
    + it's possible to only insert data in specific columns

```sql
INSERT INTO Customers (CustomerName, ContactName, Address, City, PostalCode, Country)
VALUES ('Cardinal', 'Tom', 'Skagen', 'Stavanger', '4006', 'Norway');
```

## SQL *UPDATE* Statement

- UPDATE statement is used to update existing records in a table
    + WHERE clause specifies with record or records that should be updated
        * if ommitted, all records will be updated

Update the customer "Alfred" with a new contact person and city:
```sql
UPDATE Customers
SET ContactName='James', City='Hamburg'
WHERE CustomerName='Alfred'
```


## SQL *DELETE* Statement

- DELETE is used to delete rows in a table
    + WHERE clause specifies with record or records to delete
        * if ommitted, all records will be deleted

Delete the customer "Alfred" from the "Customers" table:
```sql
DELETE FROM Customers
WHERE CustomerName='Alfred' AND ContactName='Maria';
```

Delete all rows without deleting the table:
```sql
DELETE FROM Customers;
```

```sql
DELETE * FROM Customers;
```
