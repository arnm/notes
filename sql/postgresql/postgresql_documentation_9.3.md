# PostgreSQL Documentation 9.3

## Preface

### 1. What is PostgreSQL?

- PostgreSQL
    + is an object-relational database management system
    + is based on POSTGRES, Version 4.2
    + was developed Berkley C.S Department
    + is open-source
        * can be used, modified, and distributed by anyone for any purpose
    + offers a large part of the SQL standard and many modern features
        * complex queries
        * foreign keys
        * triggers
        * updatable views
        * transactional integrety
        * multiversion concurrency control
    + can be extended by the user by adding new
        * data types
        * functions
        * operators
        * aggregate functions
        * index methods
        * procedural languages

### Ch.1 Getting Started

#### 1.2 Architectural Fundamentals

- PostgreSQL uses a client/server model
    + client and server could be on different hosts
        * they communicate over a TCP/IP network connection

- A PostgreSQL session consists of
    + A server process
        * called **postgres**
        * manages the database files
        * accepts connections to the DB from client apps
        * performs DB actions on behalf of the clients
    + The user's client app
        * wants to perform DB operations
        * could be text-oriented, GUI, web server, DB maintenace tool

- PostgreSQL Server
    + can handle multiple concurrent connections from clients
        * forks a new process for each connection
            - client and the new server process communicate without intervention by the original *postgres* process
        * master server process is always running
            - waiting for new client connections
            - client and associated server process come and go

#### 1.3 Creating a Database

- **postgres** server must already be running

**create a database with the name 'mydb'**

```shell
$ createdb mydb
```

- PostgreSQL database naming:
    + database names must have an alphabetic first character
    + a convenient choice is to name a database with your current user name
        * many tools assume that databse name as the default

**create a database with the current user name**

```shell
$ createdb
```

**Remove a database named 'mydb'**

```shell
$ dropdb mydb
```

#### 1.4 Accessing a Database



