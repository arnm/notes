# Programming Clojure, 2nd Edition

## 1. Getting Started

**Clojure**

- is a functional language
- is a Lisp for the JVM
- has special features for dealing with concurrency

**The Key Concepts for Thinking in Clojure**

- **Simplicity**
    + a thing is simple if it is not compound
    + simple components do what their suppose to do, and no more
    + irrelevant complexity becomes dangerous complexity
- **Power**
    + sufficiency to the tasks we want to undertake
    + build on top of a capable and widely deployed foundation e.g the JVM
    + your tools must provide you with unrestricted access to that power

### 1.1 Why Clojure?

All of the distinctive features in Clojure are there to provide simplicity, power, or both.

- **Simple Features**
    + *Functional Programming*
        * it isolates calculation from state and identity
        * functional programs are easy to understand, write, test, optimize, and parallelize
    + *Lisp*
        * it seperates reading from evaluation
        * language syntax is made from a small number of orthogonal parts
        * syntactic abstraction captures design patters
        * S-expressions are XML, JSON, and SQL as they should have been
    + *Clojure's Time Model*
        * separating values, identities, state, and time
        * programs can perceive and remember information, without fear of mutability
    + *Protocols*
        * seperating polymorphism from derivation
        * you get safe, ad hoc extensibility of type and abstractions
- **Power Features**
    + *Clojure's Java Interop Forms*
        * gives direct access to the semantics of Java Platform
        * programs can have performance and semantic equivalence to Java
    + *Lisp*
        * provides a compiler and macro system at runtime
        * late-bound decision making and easy DSLs

#### Clojure is Elegant

- **Clojure is Concise**
    + programs are small
        * cheaper to build, deploy, and maintain
        * Possible because of higher order functions

Higher Order Functions
: Functions that take functions as arguments and/or returns functions as results.
    
#### Clojure is Lisp Reloaded

- **Clojure's Challenges as a Lisp**
    + Demonstrate to Lisp programmers that Clojure embraces the Lisp concepts
    + Win support from the broader community

#### Why Lisp?

**Lisps have:**

- have a tiny language core
- almost no syntax
- powerful macro facilities

**These features are good because:**

- they allow users to bend Lisp to meet their needs
    + don't have to write repetitive, error-prone workarounds
- you can add your own language features with macros
    + Clojure itself is build out of macros
    + don't have to petition the language implementers to add features

**Unique Advantages of Lisp**

- ability to reprogram the language from within the language
- Lisp is homoiconic
    + makes it easy for programs to write other programs
- The whole language is there, all the time

Homoiconicity
: A property of a programming language in which the program structure is similar to its syntax. That is, Lisp code is just Lisp data.

**Lisp Syntax**

- eliminates rules for operator precedence and associativity
- for beginners, Lisp's fixation on parentheses as core data structure can be a downside

#### Lisp, with Fewer Parentheses
