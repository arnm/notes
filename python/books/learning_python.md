# Learning Python, 5th Edition

## Part I. Getting Started

### Chapter 1. A Python Q&A Session

#### Why Do People Use Python?

- **Software Quality**
    + Python's focus on readability, coherence, and software quality
    + Python code is designed to be readable, and hence reuseable and maintainable
    + Python's uniformity makes it easy to understand
    + Python has support for Object Oriented and Functional programming styles
- **Developer Community**
    + Python boosts developer productivity
    + Python code is typically 1/3 to 1/5 the size of C++ or Java
        * less typing, less to debug, less to maintain
    + no need to compile, Python programs run immediately
- **Program Portability**
    + most Python programs run unchanged on all major platforms
- **Support Libraries**
    + comprehensive standard library
    + large amount of third-party libraries
- **Component Integration**
    + Python scripts can easily communicate with other parts of an application
        * allows Python to be used as a product customization and extension tool
        * Python can be called from C/C++
        * Python can integrate with Java and .NET components
- **Enjoyment**
    + Python's ease of use and built-in toolset makes programming more pleasureable

```python
>>> import this
'''The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!'''
```


#### Is Python a "Scripting Language"?

- Python is a general-purpose programming language
    + it is often applied in scripting roles
    + blends prcedural, functional, and object-oriented paradigms

**People associate scripting languages with:**

- Shell tools
    + tool for coding OS-oriented scripts
    + Python can and do serve this role, but it's only one of its many domains
- Control Language
    + "Glue" layer used to control and direct other application components
    + Python can and do serve this role, but it's only one of its many domains
- Ease of use
    + simple language used for quickly coding tasks
    + Python is not just for simple tasks
        * it makes tasks simple by its ease of use and flexibility
        * also allows programs to scale up in sophistication as needed

Scripting
: Probably best used to describe the rapid and flexible mode of development that Python supports, rather than a particular application domain.

#### What are the Downsides of Using Python?

- **Execution Speed**
    + may not always be as fast as that of fully compiled and lower-level languages
    + standard implementation of Python compile source code statements into byte code
        * provides portability because it's platform-independent
        * Python is not normally compiled all the way down to binary machine code
    + PyPy system can achive 10x - 100x speedup on some code by compiling further
        * but PyPy is a seperate, alternative implementation of Python

