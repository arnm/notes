# Introduction to Algorithms, 3rd Edition

## Foundations

### 1 The Role of Algorithms in Computing

- **Chapter Topics:**
    + What are algorithms?
    + Why is the study of algorithms worthwhile?
    + What is the role of algorithms relative to other technologies in computers?

#### 1.1 Algorithms

Algorithm
: Any well-defined computational procedure that takes some value, or set of values, as **input** and produces some value, or set of values, as **output**.
: A sequence of computational steps that transforms the input into the output.
: A tool for solving a well-specified **computational problem**.

- **Computational Problem**
    + the statement of the problem specifies the desired input/output relationship
    + the algorithm descrives a specific computational procedure for acheiving that input/ouput relationship

**Sorting Problem Example**

Sort a sequence of numbers into increasing order.

**Input:** A sequence of numbers (\\(a_{1}, a_{2}, \\dotsc, a_{n}\\))
**Output:** A permutation (reordering) (\\(a'\_{1}, a'\_{2}, \\dotsc, a'\_{n}\\)) such that \\(a'\_{1} \\le a'\_{2} \\le \\dotsc \\le a'\_{n} \\)

Given (31, 41, 59, 26, 41, 58), a sorting algorithm returns as output the sequence (26, 31, 41, 58, 59).

Instance of a Problem
: Consists of the input (satisfying whatever constraints are imposed in the problem statement) needed to compute a solution to the problem.

- **Sorting**
    + is a fundamental operation in computer science
    + used as intermediate step in many programs
    + has a large number of algorithms available

- **Determining which algorithm is best for an application depends on:**
    + the number of items to be sorted
    + the extent to which items are already sorted
    + possible restrictions on the item values
    + architecture of the computer
    + kinds of storage devices to be used (main memory, disks, or even tapes)

Correct Algorithm
: For every input instance, it halts with the correct output.

A correct algorithm **solves** the given computational problem.

- **Incorrect Algorithm**
    + might not halt at all on some input instances
    + might halt with incorrect answer
    + can sometimes be useful if we can control their error rate

- **How to specify an algorithm**
    + the specification must provide a precise description of the computation procedure to be followed
        * in English
        * as a computer program
        * as a hardware design

#### 1.2 Algorithms as a Technology

- Would you have any reason to study algorithms if computers were infinitely fast and computer memory was free?
    + you need to be able to demonstrate your implementation produces the correct answer
    + any correct algorithm would be sufficient

- computers are not infinetely fast or have free memory
    + computing time and space in memory are bounded resources
    + algorithms which are efficient in terms of time and space are needed

##### Efficiency

- Different algorithms devised to solve the same problem
    + often differ dramatically in efficiency
    + efficiency differences can be more significant than differences due to hardware

- **Insertion Sort**
    + Takes time roughly equal to c<sub>1</sub>*n*<sup>2</sup> to sort *n* items, where c<sub>1</sub> is a constant that does not depend on *n*.
    + Takes time proportional to *n*<sup>2</sup>

- **Merge Sort**
    + takes time roughly equal to c<sub>2</sub>*n* lg *n*, where lg *n* stands for log<sub>2</sub>*n* and c<sub>2</sub> is another constant that also does not depend on *n*.

- *Constant Factors*
    + can have far less of an impact on the running time than the dependence on the input size *n*.

**Ex.** Sort 10 millions numbers with each algorithms and compare.

|                    | Computer A (Insertion Sort) |   Computer B (Merge Sort)   |
|--------------------|-----------------------------|-----------------------------|
| Instruction/Second | 10<sup>10</sup>             | 10<sup>7</sup>              |
| Time Complexity    | 2*n*<sup>2</sup>            | 50*n*lg*n*                  |
| Total Time         | 20,000 seconds (5.5 hours)  | 1,163 seconds (< 20 minutes) |

- Example analysis
    + using algorithms whose running time grows more slowly, even with a slower computer, it will usually run much faster then the other algorithm whose running time rapidly increases
    + As the problem size increases, so does the relative advantage of merge sort

##### Algorithms and other technologies

- Algorithms should be considered as a technology
    + choosing efficient algorithms is as important as choosing fast hardware
    + many of the contemporary technologies rely heavily on algorithms
        * GUIs
        * Web technologies
        * networks
        * compiles, interpreters, assemblers, etc.
    + even if at the application level no algorithmic content is used, it still relies on heavily on algorithms
    + problems are getting larger and larger and algorithms need to evolve to be able to tackle those new problems


### 2 Getting Started

- **Chapter Topics:**
    + framework to think about the design and analysis of algorithms
    + Insertion sort algorithm
    + Divide-and-conquer algorithm design
    + Merge sort algorithm

#### 2.1 Insertion Sort

**Sorting Problem Example**

Sort a sequence of numbers into increasing order.

**Input:** A sequence of numbers (\\(a_{1}, a_{2}, \\dotsc, a_{n}\\))
**Output:** A permutation (reordering) (\\(a'\_{1}, a'\_{2}, \\dotsc, a'\_{n}\\)) such that \\(a'\_{1} \\leq a'\_{2} \\leq \\dotsc \\leq a'\_{n} \\)

Keys
: The numbers that we wish to sort.

Pseudocode
: Similar to "real" code but employs whatever expressive method is most clear and concise to specify a given algorithm (e.g English). It also does not concern itself with issues like data abstraction, modularity, or error handling.

- **Insertion Sort**
    + efficient for sorting small number of elements

```python
def insertion_sort(numbers):
    for i in range(1, len(numbers)):
        key = numbers[i]
        j = i
        while j > 0 and numbers[j - 1] > key:
            numbers[j] = numbers[j - 1]
            j -= 1
        numbers[j] = key
    return numbers
```
